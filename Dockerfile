FROM golang:1.21.0-alpine3.18 as go_api

RUN apk add --no-cache git
WORKDIR /app

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    GOSUMDB=off

RUN apk --no-cache add tzdata

COPY /golang/go.mod .
COPY /golang/go.sum .

RUN go mod download
COPY /golang/ .

RUN go build -o apiserver .
# CMD ["./server"]

########################################################################################################################

# Install dependencies only when needed
FROM node:lts-alpine3.18 AS node_package
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app

# Install dependencies based on the preferred package manager
COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./
RUN \
  if [ -f yarn.lock ]; then yarn --frozen-lockfile; \
  elif [ -f package-lock.json ]; then npm ci; \
  elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i --frozen-lockfile; \
  else echo "Lockfile not found." && exit 1; \
  fi

# Rebuild the source code only when needed
FROM node:lts-alpine3.18 AS builder_node
WORKDIR /app
COPY --from=node_package /app/node_modules ./node_modules
COPY . .

# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry during the build.
# ENV NEXT_TELEMETRY_DISABLED 1

RUN yarn build

# If using npm comment out above and use below instead
# RUN npm run build

# Production image, copy all the files and run next
FROM node:19.0.0-alpine3.16 AS nextjs_web
WORKDIR /app

ENV NODE_ENV production
# Uncomment the following line in case you want to disable telemetry during runtime.
# ENV NEXT_TELEMETRY_DISABLED 1

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

COPY --from=builder_node /app/public ./public

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder_node --chown=nextjs:nodejs /app/.next/standalone ./
COPY --from=builder_node --chown=nextjs:nodejs /app/.next/static ./.next/static

USER nextjs

# EXPOSE 3000

# ENV PORT 3000

# CMD ["node", "server.js"]

########################################################################################################################

FROM nginx:1.25.1-alpine3.17
WORKDIR /var/www/html

RUN apt-get update && \
    apt-get -y upgrade

#RUN apt-get -y install software-properties-common
RUN apt-get -y install nano telnet supervisor curl git

COPY ./start_project.sh .

RUN chmod -R 775 ./start_project.sh

COPY --from=nextjs_web /app/ ./website/
COPY --from=go_api /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=go_api /app/apiserver ./api/apiserver
COPY ./vhost.conf /etc/nginx/conf.d/default

EXPOSE 80
ENV TZ=Asia/Bangkok
#CMD /etc/init.d/nginx start && tail -f /var/log/nginx/access.log
#CMD /etc/init.d/nginx start && ./api/apiserver && node ./website/server.js && tail -f /var/log/nginx/access.log
ENTRYPOINT ["./start_project.sh"]